# Billy

>La capacité de générer de la connaissance, et donc de la valeur, à partir d’un ensemble de données
brutes est centrale dans notre monde connecté où des quantités gigantesques de données sont produites
à chaque instant.


Billy est notre solution pour faciliter la manipulation de grands ensembles de données, en java.
Cette bibliothèque vous permettra de représenter, sélectionner et utiliser vos ensembles de données bien plus efficacement qu'en utilisant les fonctions natives de Java.



Billy apporte une nouvelle structure de données qui vous permettra de manipuler vos tableaux de façon beaucoup plus intuitive, le DataFrame :

![alt text](https://pandas.pydata.org/docs/_images/01_table_dataframe.svg "Dataframe representation")

###### *inpiré par [pandas](https://pandas.pydata.org/)*

## Fonctionnalités


#### 1. Création simple :

* Pour créer un DataFrame, il vous suffit de fournir vos données dans une liste de liste d'objets. 
Les premiers objets contenus dans ces listes devront être le nom de la colonne dans le DataFrame.

    ```java
    List<List<Object>> data = new LinkedList<>();
    List<Object> column1 = new LinkedList<>();
    List<Object> column2 = new LinkedList<>();
    column1.add("Name");
    column1.add("Tanguy");
    column1.add("Mathieu");
    column2.add("Grade");
    column2.add(17);
    column2.add(17);
    data.add(column1);
    data.add(column2);
    DataFrame dataFrame = new DataFrame(data);
    ```

* Un second constructeur vous permet de créer un DataFrame directement depuis un fichier au format CSV.
Pas besoin de spécifier de type en début de colonnes, Billy s'en charge pour vous.
Vous n'avez qu'à télécharger votre fichier de données formaté et vous pouvez le manipuler directement.

    ```java
    DataFrame dataFrame = new DataFrame("path/to/my/file/data.csv");
    ```

#### 2. Affichage modulaire :
Pour afficher un DataFrame vous avez 3 options :

* Afficher le DataFrame dans son intégralité.

    ```java
    dataFrame.printAll();
    ```
    Ou simplement : 
    ```java
    System.out.println(dataFrame);
    ```
    Et vous obtiendrez un affichage console :
    ```bash
        -------------------
        |   Name  | Grade |
    -----------------------
    | 0 |  Tanguy |   17  |
    | 1 | Mathieu |   17  |
    -----------------------
    ```
* Afficher seulement les premières lignes, en choisissant le nombre de lignes à afficher :
    
    ```java
    dataFrame.printFirst(1);
    ```
    ```bash
          -------------------
          |   Name  | Grade |
    -------------------------
    |  0  |  Tanguy |   17  |
    | ... |   ...   |  ...  |
    -------------------------
    ```

* Afficher seulement les dernières lignes, en choisissant là aussi, le nombre de lignes à afficher :
    
    ```java
    dataFrame.printLast(1);
    ```
    ```bash
          -------------------
          |   Name  | Grade |
    -------------------------
    | ... |   ...   |  ...  |
    |  1  | Mathieu |   17  |
    -------------------------
    ```


#### 3. Sélection adaptée :
Pour sélectionner les données qui vous intéressent lors de votre traitement,
 nous mettons à votre disposition 3 options :
 * Une sélection simple par ligne :

    ```java
    List<Integer> selectedRows = Arrays.asList(0);
    DataFrame newDataFame = dataFrame.selectByRow(selectedRows);
    System.out.println(newDataFrame);
    ```
    ```bash
          -------------------
          |   Name  | Grade |
    -------------------------
    |  0  |  Tanguy |   17  |
    -------------------------
    ```
    ou par colonne :
    ```java
    List<String> selectedCols = Arrays.asList("Grade");
    DataFrame newDataFame = dataFrame.selectByColumn(selectedCols);
    System.out.println(newDataFrame);
    ```
    ```bash
       ---------
       | Grade |
   -------------
   | 0 |   17  |
   | 1 |   17  |
   -------------
    ```

* Une sélection plus avancée, basée sur les indices uniquement. 
Vous pouvez effectuer votre sélection de différentes façons.
Il suffit de renseigner soit : 
    - Un indice 
    - Une liste d'indices
    - Une chaîne de caractères représentant les données sélectionnées.
    - Une combinaison de ces paramètres
    ```java
    DataFrame newDataFrame = dataFrame.iloc(selectedRows,selectedCols);
    DataFrame newDataFrame = dataFrame.iloc(0,selectedCols);
    DataFrame newDataFrame = dataFrame.iloc(selectedRows,1);
    DataFrame newDataFrame = dataFrame.iloc(0,2);
    DataFrame newDataFrame = dataFrame.iloc("0:2",":");
    DataFrame newDataFrame = dataFrame.iloc(":1","1:");
    DataFrame newDataFrame = dataFrame.iloc("1:-1","-2:-1");
    ```

* Un autre outil de sélection avancée permettant de rechercher des lignes, qui correspondent à vos critères.
Il suffit de renseigner le nom de la colonne à tester et l'expression attendue, comme suit :

    ```java
    DataFrame newDataFrame = dataFrame.where("Age",">",21);
    DataFrame newDataFrame = dataFrame.where("First Name","==","Mathieu");
    ```

Il est à noter que comme ces méthodes renvoient toutes de nouveaux DataFrame, 
il est possible de chaîner les appels, par exemple : 

```java
List<String> selectedCols = Arrays.asList("Grade");
DataFrame newDataFrame = dataFrame.selectByColumn(selectedCols).iloc("0:1",0).where("Age",">",21);
```

Maintenant que vous savez sélectionner les données qui vous intéressent, vous pouvez être amené 
à les rassembler. Une méthode est prévue pour ça :

```java
DataFrame mergedDataFrame = dataFrame1.merge(dataFrame2);
```
`dataFrame1` :
```bash
    ----------------------
    | First name | Grade |
--------------------------
| 0 |   Tanguy   |   17  |
| 1 |   Mathieu  |   17  |
--------------------------
```

`dataFrame2` :
```bash
    ----------------------
    | First name | Grade |
--------------------------
| 0 |   Tanguy   |   17  |
| 1 |   Mathieu  |   17  |
--------------------------
```

`mergedDataFrame` :
```bash
    ----------------------------------------
    | First name | Grade | Age | Last Name |
--------------------------------------------
| 0 |   Tanguy   |   17  |  24 |   Court   |
| 1 |   Mathieu  |   17  |  22 |  Gautron  |
--------------------------------------------
```

Lors d'une fusion, les colonnes portant le même label, dans les deux DataFrames seront fusionnées.

#### 4. Statistiques à portée de main :
Nous avons mis à votre disposition plusieurs fonctions vous permettant d'obtenir des statistiques sur vos
Dataframe. Dans toutes les fonctions, la structure est la même : fournir en paramètre une liste de labels correspondant 
aux colonnes sur lesquelles vous souhaitez réaliser vos opérations statistiques.

Nous précisons tout de même que ces opérations ne sont possibles que sur des colonnes de type réel ou entier.
```java
List<String> listeDesLabels = new ArrayList();
listeDesLabels.add("data");
listeDesLabels.add("age");
List<Float> listeDesMax = dataFrame.maxByColumn(listeDesLabels);
List<Float> listeDesMin = dataFrame.minByColumn(listeDesLabels);
List<Float> listeDesMoyennes = dataFrame.averageByColumn(listeDesLabels);
List<Float> listeDesSommes = dataFrame.sumByColumn(listeDesLabels);
```

#### 5. Outils standard :
Bien sûr, le DataFrame met à disposition des méthodes standard de Java pour faciliter son utilisation.

```java
dataFrame.getNbColumn();
dataFrame.getNbRow();
dataFrame.toString();
dataFrame.iterator();
dataFrame.equals(dataFrame1);
```

## Choix d'implémentation 

Pour mener à bien ce projet nous avons dû faire les choix suivant :
* Pour la création d'un DataFrame, nous devions permettre aux utilisateurs de renseigner
 des données non typées mais qui illustre la façon dont le DataFrame doit être réalisés.
 Pour ce faire nous choisis de prendre une `List<List<Object>>` en paramètre.
 Il faut aussi que le premier element des listes internes soit de type `String` pour initialiser 
 les labels des colonnes.

* Lors de la création d'un DataFrame à partir d'un fichier CSV, nous avons choisi de ne pas contraindre
 à indiquer le type de chaque colonne à la première ligne du fichier. On se charge donc de parser les données
 pour en déduire le type à instancier.
 
* Nous acceptons d'avoir des valeurs manquantes pour prendre en compte un maximum de cas possible.

* Nous autorisons la création d'une colonne avec uniquement des valeurs manquantes, tant qu'elle est labellisé.

* Pour indiquer une valeur manquante dans un fichier CSV, elle ne doit pas être vide, mais remplacée par "NA".
Nous avons défini ceci en nous basant sur des fichiers CSV trouvés sur internet ([Kaggle](https://www.kaggle.com/datasets))

* À la création d'un DataFrame, si la taille des colonnes de ce DataFrame sont différentes, alors on complète
 les plus petites avec des symboles de valeur manquante ("NA") pour avoir une taille de colonne uniforme au
 sein d'un même dataFrame.

* Nous avons eu quelques soucis pour typer correctement les colonnes à leurs créations depuis un fichier CSV. 
 Comme ces-dernières ne doivent contenir qu'un seul et unique type de donnée et que nous cherchons les types à partir
 d'une `String` lue dans le fichier CSV, il a fallu faire des compromis. Ainsi, si la première valeur trouvée d'une 
 colonne est de type `String` alors la colonne sera de type `String`, et ce, même si toutes les autres valeurs de cette
 colonne sont de type `Integer` ou `Float`. Pour s'abstraire de ceci, il aurait fallu trouver le type prédominant dans 
 la colonne pour juger de la validité de la colonne, rajoutant ainsi, une complexité que nous ne souhaitions pas
 pour un projet orienté DevOps.

* Pour gérer l'affichage, nous avons choisis d'exporter cette responsabilité dans une
 nouvelle classe : le `DataFramePrinter`.

* La méthode `merge` de la classe DataFrame, ne fusionne pas les lignes. En effet, si on souhaite fusionner deux
DataFrames contenant par exemple les colonnes `Nom` , `Age` pour un dataFrame et `Nom` , `Prénom` pour l'autre. 
On pourrait alors être tenté de considérer que les lignes ayant la même valeur dans les deux dataFrames à la colonne
`Nom` décrivent la même personne et donc de les fusionner. Mais dans le cas ou plusieurs personnes aurait le même nom
on risquerait de mélanger les données entre elles. Pour prendre ceci en compte, il faudrait mettre en place un
mécanisme de reconnaissance bien plus complexe, que nous avons choisis encore une fois d'éviter.



## Images Docker
Pour tester notre bibliothèque, nous vous fournissons deux images docker :

* `docker run tacourt/billy:latest` , qui vous permet d'éxécuter les tests unitaires.

*  `docker run tacourt/billy:demo`, qui lancera un scénario de test montrant les fonctionnalités implémentées.

Ces images sont stockées et rendues publique sur ce [repository docker hub.](https://hub.docker.com/repository/docker/tacourt/billy)

## Feedback
 Tout d'abord, ce projet a été très enrichissant et nous a appris beaucoup, notamment au niveau des
 outils de CI tels que gitlab CI. 
 
 Néanmoins, n'ayant jamais mis en place de CI, nous avons trouvé la mise en place des pipelines assez compliquée.
 En effet, nous avions de nombreuses erreurs qui mettaient à l'echec notre pipeline alors que tout se passait bien localement.
 Il a donc fallu faire de nombreux push sur le repo distant pour trouver l'erreur.