import java.io.IOException;

public class MainTest {

    public static void main(String[] args) throws EmptyFileException, IOException, WrongTypeException, DimensionErrorException, WrongFormatException {
        //Création du dataFrame
        System.out.println("Création d'un dataFrame complet contenant les 100 meilleurs films :");
        DataFrame dataFrame = new DataFrame("src/main/resources/movies.csv");
        dataFrame.printFirst(5);
        dataFrame.printLast(5);

        System.out.println("");
        System.out.println("Voici les films parus après l'année 2000 dont le score dépasse 8 :");
        dataFrame.where("Year",">",2000).where("Score",">",8).printAll();

        System.out.println("");
        System.out.println("Voici tous les films de Christopher Nolan, en affichant uniquement les colonnes de Classement, Titre, année et Score :");
        dataFrame.where("Director","==","Christopher Nolan").iloc(":","0:3").printAll();

        System.out.println("");
        System.out.println("Voici les films dont le classement est compris entre 30 et 45 et dont le Metascore est supérieur ou égal à 70 :");
        dataFrame.iloc("30:45",":").where("Metascore",">=",70).printAll();

        System.out.println("");
        System.out.println("Voici le film ayant le meilleur metascore :");
        dataFrame.where("Metascore","=",dataFrame.maxByColumn("Metascore")).printAll();

        System.out.println("");
        System.out.println("La moyenne de des année du tableau est de  :");
        System.out.println(dataFrame.averageByColumn("Year"));

        System.out.println("");
        System.out.println("Le film le plus court a une durée de :");
        System.out.println(dataFrame.minByColumn("Runtime"));

        System.out.println("En fusionnant ce DataFrame :");
        DataFrame df1 = dataFrame.iloc("0:5","0:3");
        df1.printAll();
        System.out.println("Avec celui-ci :");
        DataFrame df2 = dataFrame.iloc("0:5","4:5");
        df2.printAll();
        System.out.println("On obtient le DataFrame suivant :");
        df1.merge(df2).printAll();

        System.out.println("En fusionnant ce DataFrame :");
        DataFrame df3 = dataFrame.iloc("0:5","0:3");
        df3.printAll();
        System.out.println("Avec celui-ci :");
        DataFrame df4 = dataFrame.iloc("20:25","0:5");
        df4.printAll();
        System.out.println("On obtient le DataFrame suivant :");
        df3.merge(df4).printAll();
    }
}
