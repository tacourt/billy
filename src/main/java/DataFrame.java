import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class DataFrame implements Iterable<Column> {
    private List<Column> columns;


    // ----------- Constructeurs ------------------

    // Constructeur à partir d'une structure simple
    public DataFrame(List<List<Object>> array) throws WrongTypeException, DimensionErrorException, EmptyFileException{
        initData(array);
    }

    // Constructeur à partir d'un fichier CSV
    public DataFrame(String fileName) throws WrongTypeException, DimensionErrorException, EmptyFileException, IOException {
        List<List<Object>> array = readCSV(fileName);
        initData(array);
    }


    // -------------- Méthodes ------------------

    // -------------- Méthodes d'initialisation ------------------


    // Méthode de remplissage des données du dataframe à partir des données simples fournies en entrée
    protected void initData(List<List<Object>> array) throws WrongTypeException, DimensionErrorException , EmptyFileException{
        List<Column> cols = new LinkedList<>();

        // On commence par rechercher la colonne la plus grande
        int maxColumnSize = array.get(0).size();
        for( List<Object> colp : array ){
            if( colp.size() > maxColumnSize){
                maxColumnSize = colp.size();
            }
        }
        maxColumnSize--;

        // On peut alors compléter toutes les colonnes plus petites
        // pour avoir des colonnes de taille uniforme au sein du dataFrame.
        for( List<Object> col : array ){
            Column c = new Column(col);
            c.extendToFitSize(maxColumnSize);
            cols.add(c);
        }

        this.columns = cols;
    }




    // Methode chargée de lire le fichier CSV fourni en paramètre et de charger les données qu'il contient en mémoire dans une structure
    // de données simple. Utilise des Regex pour reconnaître les types des données du fichier.
    protected List<List<Object>> readCSV(String fileName) throws EmptyFileException, IOException {
        List<List<Object>> array = new ArrayList<>();

        BufferedReader br = Files.newBufferedReader(Paths.get(fileName));

        // On commence par remplir les labels des colonnes
        String line;
        if((line = br.readLine()) != null){
            String[] labels = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
            for( String label : labels){
                List<Object> column = new ArrayList<>();
                column.add(label);
                array.add(column);
            }
        }else{
            throw new EmptyFileException("Le fichier est vide");
        }

        // Puis on remplit les données en elle-mêmes
        while ((line = br.readLine()) != null) {
            String[] values = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
            for( int i=0; i<values.length; i++) {
                Object typedValue = instanciateMatchingType(values[i]);
                array.get(i).add(typedValue);
            }
        }

        return array;
    }




    // Méthode chargée d'instancier la valeur donnée en paramètre via une chaine de caractères dans
    // le bon type de donnée.
    protected Object instanciateMatchingType(String value){
        String floatRegex ="^-?[0-9]+\\.[0-9]*$";
        String intRegex ="^-?[0-9]*$";
        String nullValueRegex = "^NA$";

        if(value.matches(floatRegex)){
            return new Float(value);
        }else if (value.matches(intRegex)){
            return new Integer(value);
        }else if (value.matches(nullValueRegex)){
            return "null";
        } else {
            return value;
        }

    }

    // -------------- Méthodes d'affichage ------------------

    // Méthode d'affichage d'un dataFrame complet
    public void printAll(){
        DataFramePrinter dfp = new DataFramePrinter(this);
        System.out.println(dfp.getAllDataString());
    }

    // Méthode d'affichage des "nbRowsToPrint" premières lignes d'un dataFrame.
    public void printFirst(Integer nbRowsToPrint){
        DataFramePrinter dfp = new DataFramePrinter(this);
        System.out.println(dfp.getFirstRowsString(nbRowsToPrint));
    }

    // Méthode d'affichage des "nbRowsToPrint" dernières lignes d'un dataFrame.
    public void printLast(Integer nbRowsToPrint){
        DataFramePrinter dfp = new DataFramePrinter(this);
        System.out.println(dfp.getLastRowsString(nbRowsToPrint));
    }


    // -------------- Méthodes statistiques ------------------


    // Méthode donnant la valeur maximale contenue dans les colonnes correspondant aux labels fournis en paramètre.
    public List<Float> maxByColumn(List<String> labels) throws WrongTypeException, DimensionErrorException, EmptyFileException {
        DataFrame df = this.selectByColumn(labels);
        Iterator<Object> it;
        List<Float> res = new ArrayList<>();
        for(Column c : df.columns) {
            it = c.iterator();
            Object o = it.next();
            Float max, value;
            if (o instanceof String) {
                throw new WrongTypeException("Erreur: Il semblerait que certains labels correspondent à des colonnes de String");
            } else {
                if(o instanceof Integer) {
                    max = ((Integer) o).floatValue();
                } else {
                    max = (Float)o;
                }
            }
            while (it.hasNext()) {
                o = it.next();
                if(!o.equals("null")){
                    if(o instanceof Integer) {
                        value = ((Integer) o).floatValue();
                    } else {
                        value = (Float)o;
                    }
                    if(max < value) {
                        max = value;
                    }
                }

            }
            res.add(max);
        }
        return res;
    }



    // Méthode donnant la valeur minimale contenue dans les colonnes correspondant aux labels fournis en paramètre.
    public List<Float> minByColumn(List<String> labels) throws WrongTypeException, DimensionErrorException, EmptyFileException {
        DataFrame df = this.selectByColumn(labels);
        Iterator<Object> it;
        List<Float> res = new ArrayList<>();
        for(Column c : df.columns) {
            it = c.iterator();
            Object o = it.next();
            Float min, value;
            if (o instanceof String) {
                throw new WrongTypeException("Erreur: Il semblerait que certains labels correspondent à des colonnes de String");
            } else {
                if(o instanceof Integer) {
                    min = ((Integer) o).floatValue();
                } else {
                    min = (Float)o;
                }
            }
            while (it.hasNext()) {
                o = it.next();
                if(!o.equals("null")){
                    if(o instanceof Integer) {
                        value = ((Integer) o).floatValue();
                    } else {
                        value = (Float)o;
                    }
                    if(value < min) {
                        min = value;
                    }
                }
            }
            res.add(min);
        }
        return res;
    }




    // Méthode donnant la moyenne des valeurs contenues dans les colonnes correspondant aux labels fournis en paramètre.
    public List<Float> averageByColumn(List<String> labels) throws WrongTypeException, DimensionErrorException, EmptyFileException {
        DataFrame df = this.selectByColumn(labels);
        Iterator<Object> it;
        List<Float> res = new ArrayList<>();
        float size = this.getNbRow();
        for(Column c : df.columns) {
            it = c.iterator();
            Object o = it.next();
            Float avergage, value;
            if (o instanceof String) {
                throw new WrongTypeException("Erreur: Il semblerait que certains labels correspondent à des colonnes de String");
            } else {
                if(o instanceof Integer) {
                    avergage = ((Integer) o).floatValue();
                } else {
                    avergage = (Float)o;
                }
            }
            while (it.hasNext()) {
                o = it.next();
                if(!o.equals("null")){
                    if(o instanceof Integer) {
                        value = ((Integer) o).floatValue();
                    } else {
                        value = (Float)o;
                    }
                    avergage += value;
                }
            }
            res.add(avergage/size);
        }
        return res;
    }

    // méthodes de statistique simplifié pour un seul label


    public Float minByColumn(String label) throws WrongTypeException, DimensionErrorException, EmptyFileException {
        List<String> selected = new ArrayList<>();
        selected.add(label);
        return this.minByColumn(selected).get(0);
    }

    public Float maxByColumn(String label) throws WrongTypeException, DimensionErrorException, EmptyFileException {
        List<String> selected = new ArrayList<>();
        selected.add(label);
        return this.maxByColumn(selected).get(0);
    }

    public Float averageByColumn(String label) throws WrongTypeException, DimensionErrorException, EmptyFileException {
        List<String> selected = new ArrayList<>();
        selected.add(label);
        return this.averageByColumn(selected).get(0);
    }


    // -------------- Méthodes de sélection ------------------


    // Méthode permettant de fusionner 2 dataframes, pour n'en former qu'un. Les colonnes ayant le même label dans
    // les deux dataFrames sont fusionnées.
    public DataFrame merge(DataFrame externalDataFrame) throws DimensionErrorException, EmptyFileException, WrongTypeException {

        // Initialisation des variables
        boolean isInserted;
        List<String> mergedCols = new ArrayList<>();
        List<Column> newDataFrame = new ArrayList<>();

        // On commence par remplir les données avec les données contenues dans le dataFrame actuel.
        // Et on fusionne les colonnes ayant le même label que celles du dataFrame fournis, et on garde ces labels.
        for(Column c : this.columns){
            isInserted = false;
            for( Column externals : externalDataFrame.columns){
                if( externals.getLabel().equals(c.getLabel()) ){
                    newDataFrame.add( c.append(externals,c.getLabel()) );
                    mergedCols.add(externals.getLabel());
                    isInserted = true;
                }
            }
            if (!isInserted){
                newDataFrame.add(c);
            }
        }

        // On remplit le reste des données avec le second dataFrame
        // en mettant de côté les colonnes déjà remplies via fusion.
        if(mergedCols.size() >0){
            for(Column c : externalDataFrame){
                if( !mergedCols.contains( c.getLabel() ) ){
                    List<Object> initialList = new LinkedList<>();
                    initialList.add(c.getLabel());
                    for(int i =0; i< this.columns.get(0).size(); i++ ){
                        initialList.add("null");
                    }
                    List<Object> values = c.toObjectList();
                    values.remove(0);
                    initialList.addAll(values);
                    Column newCol = new Column(initialList);
                    newDataFrame.add(newCol);
                }
            }
        }else{
            for(Column c : externalDataFrame){
                newDataFrame.add(c);
            }
        }

        // On créé une structure simple, non typée pour pouvoir utiliser le constructeur.
        List<List<Object>> newData = new ArrayList<>();
        for(Column c : newDataFrame){
            newData.add(c.toObjectList());
        }

        return new DataFrame(newData);
    }



    // Méthode permettant de sélectionner un sous ensemble de données, en fournissant les lignes à garder.
    public DataFrame selectByRow(List<Integer> index) throws EmptyFileException, WrongTypeException, DimensionErrorException {
        List<List<Object>> newData = new ArrayList<>();
        Iterator<Object> it;
        for(int i=0; i<this.getNbColumn(); i++) {
            it = this.columns.get(i).iterator();
            List<Object> liste = new ArrayList<>();
            liste.add(this.columns.get(i).getLabel());
            for(int j=0; j<this.getNbRow(); j++) {
                Object o = it.next();
                if(index.contains(j)) {
                    liste.add(o);
                }
            }
            newData.add(liste);
        }
        return new DataFrame(newData);
    }



    // Méthode permettant de sélectionner un sous ensemble de données, en fournissant les labels des colonnes à garder.
    public DataFrame selectByColumn(List<String> labels) throws WrongTypeException, EmptyFileException, DimensionErrorException {
        List<List<Object>> newData = new ArrayList<>();
        for (Column column : this.columns) {
            if (labels.contains(column.getLabel())) {
                newData.add(column.toObjectList());
            }
        }
        return new DataFrame(newData);
    }



    // Méthode de sélection basée sur les indices des lignes et colonnes à sélectionner.
    public DataFrame iloc(List<Integer> rows,List<Integer> columns) throws WrongTypeException, EmptyFileException, DimensionErrorException {
        DataFrame indexDF = this.selectByRow(rows);
        List<String> colsToSelect = new LinkedList<>();
        int colIndex=0;
        for(Column c : indexDF){
            if( columns.contains(colIndex) ){
                colsToSelect.add(c.getLabel());
            }
            colIndex++;
        }
        return indexDF.selectByColumn(colsToSelect);
    }




    // Méthode de sélection basée sur l'indice d'une ligne et colonnes à sélectionner.
    public DataFrame iloc(Integer row,List<Integer> columns) throws WrongTypeException, EmptyFileException, DimensionErrorException {
        List<Integer> rowList = new LinkedList<>();
        rowList.add(row);
        return iloc(rowList,columns);
    }




    // Méthode de sélection basée sur les indices des lignes et celui de la colonne à sélectionner.
    public DataFrame iloc(List<Integer> rows,Integer col) throws WrongTypeException, EmptyFileException, DimensionErrorException {
        List<Integer> colList = new LinkedList<>();
        colList.add(col);
        return iloc(rows,colList);
    }




    // Méthode de sélection basée sur les indices de la ligne et de la colonne à selectionner.
    public DataFrame iloc(Integer row, Integer col) throws WrongTypeException, EmptyFileException, DimensionErrorException {
        List<Integer> colList = new LinkedList<>();
        colList.add(col);
        return iloc(row,colList);
    }




    // Méthode de sélection basée sur les indices des lignes et colonnes à sélectionner.
    // Permet de sélectionner des lignes en utilisant un format inspiré de python contenu dans une chaine de caractères.
    public DataFrame iloc(String selectionString, List<Integer> col) throws WrongFormatException, WrongTypeException, EmptyFileException, DimensionErrorException {
        List<Integer> indexsSelected = getIndexsFromSelectionString(selectionString,true);
        return iloc(indexsSelected,col);
    }




    // Méthode de sélection basée sur les indices des lignes et de la colonne à sélectionner.
    // Permet de sélectionner des lignes en utilisant un format inspiré de python contenu dans une chaine de caractères.
    public DataFrame iloc(String selectionString, Integer col) throws WrongFormatException, WrongTypeException, EmptyFileException, DimensionErrorException {
        List<Integer> indexsSelected = getIndexsFromSelectionString(selectionString,true);
        return iloc(indexsSelected,col);
    }




    // Méthode de sélection basée sur les indices des lignes et colonnes à sélectionner.
    // Permet de sélectionner des colonnes en utilisant un format inspiré de python contenu dans une chaine de caractères.
    public DataFrame iloc(List<Integer> rows, String selectionString) throws WrongFormatException, WrongTypeException, EmptyFileException, DimensionErrorException {
        List<Integer> indexsSelected = getIndexsFromSelectionString(selectionString,false);
        return iloc(rows,indexsSelected);
    }




    // Méthode de sélection basée sur l'indice de ligne et colonnes à sélectionner.
    // Permet de sélectionner des colonnes en utilisant un format inspiré de python contenu dans une chaine de caractères.
    public DataFrame iloc(Integer rows, String selectionString) throws WrongFormatException, WrongTypeException, EmptyFileException, DimensionErrorException {
        List<Integer> indexsSelected = getIndexsFromSelectionString(selectionString,false);
        return iloc(rows,indexsSelected);
    }




    // Méthode de sélection basée sur les indices de lignes et colonnes à sélectionner.
    // Permet de sélectionner des colonnes et des lignes en utilisant un format
    // inspiré de python contenu dans une chaine de caractères.
    public DataFrame iloc(String selectionStringRows, String selectionStringCols) throws WrongFormatException, WrongTypeException, EmptyFileException, DimensionErrorException {
        List<Integer> indexsSelectedCols = getIndexsFromSelectionString(selectionStringCols,false);
        List<Integer> indexsSelectedRows = getIndexsFromSelectionString(selectionStringRows,true);
        return iloc(indexsSelectedRows,indexsSelectedCols);
    }


    // Méthode de sélection dans un dataframe inspiré de Pandas.
    // Permet de faire une sélection en recherchant dans les valeurs d'une colonne, les lignes respectant la condition
    // exprimée par les paramètres.
    public DataFrame where(String label,String comparisonOperator, Object value) throws WrongFormatException, WrongTypeException, EmptyFileException, DimensionErrorException {

        // Sélection de la colonne concernée
        Column colToSearch = null;
        for(Column col : this.columns){
            if(col.getLabel().equals(label)){
                colToSearch = col;
            }
        }
        if(colToSearch == null){
            throw new WrongFormatException("Impossible de trouver la colonne au label donné.");
        }

        // Création du dataFrame avec recherche des lignes correspondantes
        Iterator<Object> iterator = colToSearch.iterator();
        List<Integer> selectedRows = new LinkedList<>();
        int index = 0;
        while (iterator.hasNext()){
            Object currentValue = iterator.next();
            if( compare(currentValue,comparisonOperator,value) ){
                selectedRows.add(index);
            }
            index++;
        }
        return this.selectByRow(selectedRows);
    }


    // -------------- Méthodes utilitaires ------------------


    // Méthode permettant de convertir une chaine de caractères de sélection en la liste des entiers sélectionés
    // par la chaine de caractères de sélection.
    public List<Integer> getIndexsFromSelectionString(String selectionString, boolean isAboutRows) throws WrongFormatException{

        // Initialisation des variables
        String beforeAndAfterRegex = "^-?[0-9]+:-?[0-9]+$";
        String beforeOnlyRegex = "^-?[0-9]+:$";
        String afterOnlyRegex = "^:-?[0-9]+$";
        Integer indexToStart = 0;
        Integer indexToFinish = 0;
        if(isAboutRows){
            indexToFinish = this.getNbRow()-1;
        }else{
            indexToFinish = this.getNbColumn()-1;
        }

        // On commence par analyser la chaine de caractères de sélection pour trouver
        // les indices bornant cette sélection dans le contexte du dataFrame.
        if( selectionString.matches(beforeAndAfterRegex) ){
            String[] indexs = selectionString.split(":");
            indexToStart = Integer.valueOf(indexs[0]);
            indexToFinish = Integer.valueOf(indexs[1]);
        }else if(selectionString.matches(beforeOnlyRegex)){
            indexToStart = Integer.valueOf(selectionString.substring(0,selectionString.length() -1));
        }else if(selectionString.matches(afterOnlyRegex)){
            indexToFinish = Integer.valueOf(selectionString.substring(1));
        }else if( !selectionString.equals(":") ){
            throw new WrongFormatException("La chaine de caractère de selection n'est pas correcte.");
        }

        // On gère le cas des indices négatifs qui peuvent être donnés pour parcourir le dataFrame dans l'autre sens.
        if(indexToStart < 0){
            if(isAboutRows){
                indexToStart = this.getNbRow() + indexToStart;
            }else{
                indexToStart = this.getNbColumn() + indexToStart;
            }
        }
        if(indexToFinish < 0){
            if(isAboutRows){
                indexToFinish = this.getNbRow() + indexToFinish;
            }else{
                indexToFinish = this.getNbColumn() + indexToFinish;
            }
        }
        if(indexToFinish < indexToStart){
            Integer tmp = indexToStart;
            indexToStart = indexToFinish;
            indexToFinish = tmp;
        }

        // On peut alors retourner les indices des lignes concernées
        return getListOfNumberFromTo(indexToStart,indexToFinish);
    }




    // Méthode permettant de créer simplement une liste d'entiers compris entre 2 indices.
    public List<Integer> getListOfNumberFromTo(Integer from, Integer to){
        List<Integer> intList = new LinkedList<>();
        for(int i=from; i<=to;i++){
            intList.add( i );
        }
        return intList;
    }


    // Méthode permettant d'évaluer une expression donnée via des objets et un opérateur de comparaison sous forme
    // de chaine de caractères.
    public Boolean compare(Object leftValue, String comparisonOperator, Object rightValue) throws WrongFormatException {

        // On commence par s'assurer de la cohérence des paramètres
        List<String> authorizedComparisonOperators = Arrays.asList("=","==","<",">","=>",">=","<=","=<");
        if(!authorizedComparisonOperators.contains(comparisonOperator)){
            throw new WrongFormatException("Operateur de comparaison mal formé");
        }

        if( (leftValue instanceof Integer || leftValue instanceof Float) && !(rightValue instanceof Integer || rightValue instanceof Float) ){
            return false;
        }else if( leftValue instanceof String && !(rightValue instanceof String) ){
            return false;
        }
        if(leftValue instanceof String && !(comparisonOperator.equals("==") || comparisonOperator.equals("=")) ){
            throw new WrongFormatException("L'opérateur de comparaison des chaines de caractères est = ( ou ==) uniquement.");
        }

        // Puis on effectue la comparaison en fonction du type et de l'opérateur utilisé

        if( leftValue instanceof String){
            return leftValue.equals(rightValue);
        }

        Float leftValueFloat = Float.parseFloat(leftValue.toString());
        Float rightValueFloat = Float.parseFloat(rightValue.toString());

        boolean ret = false;
        if( comparisonOperator.equals("=") || comparisonOperator.equals("==") ){
            ret = leftValueFloat.compareTo(rightValueFloat) == 0 ;
        }else if ( comparisonOperator.equals("<") ){
            ret =  leftValueFloat.compareTo(rightValueFloat) < 0;
        }else if ( comparisonOperator.equals(">") ){
            ret =  leftValueFloat.compareTo(rightValueFloat) > 0;
        }else if ( comparisonOperator.equals("<=") || comparisonOperator.equals("=<") ){
            ret =  leftValueFloat.compareTo(rightValueFloat) <= 0;
        }else if ( comparisonOperator.equals(">=") || comparisonOperator.equals("=>") ){
            ret =  leftValueFloat.compareTo(rightValueFloat) >= 0;
        }

        return ret;
    }



    // Getter retournant le nombre de colonnes contenues dans le dataFrame
    public int getNbColumn(){
        return columns.size();
    }




    // Getter retournant le nombre de lignes contenues dans le dataFrame
    public int getNbRow(){
        return columns.get(0).size();
    }




    // Méthode permettant d'obtenir un itérateur afin de parcourir le dataframe.
    @Override
    public Iterator<Column> iterator() {
        return columns.iterator();
    }




    // Méthode d'affichage d'un dataFrame
    @Override
    public String toString() {
        DataFramePrinter dfp = new DataFramePrinter(this);
        return dfp.getAllDataString();
    }




    // Méthode de comparaison de dataFrame. Permet de savoir si un dataFrame est égal à un autre, ou non.
    @Override
    public boolean equals(Object o) {
        if(!(o instanceof DataFrame)) return false;
        DataFrame cast = (DataFrame) o;
        if(!(cast.getNbRow() == this.getNbRow() && cast.getNbColumn() == this.getNbColumn())) return false;
        boolean equal = true;
        for (int i=0; i<this.columns.size(); i++) {
            equal = equal && this.columns.get(i).equals(cast.columns.get(i));
        }
        return equal;
    }



}
