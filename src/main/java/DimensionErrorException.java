public class DimensionErrorException extends Exception {
    public DimensionErrorException(String s) {
        super(s);
    }
}
