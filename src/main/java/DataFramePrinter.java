import java.util.HashMap;
import java.util.Map;

public class DataFramePrinter {
    private DataFrame dataFrame;
    private Map<String,Integer> colsMaxWidth;
    private final char colSeparator = '|';
    private final char rowSeparator = '-';

    // ----------- Constructeurs ------------------

    // Constructeur permettant de créer et initialiser l'afficheur de dataFrame.
    public DataFramePrinter(DataFrame df){
        this.dataFrame = df;
        colsMaxWidth = new HashMap<>();

        for( Column c : dataFrame){
            colsMaxWidth.put(c.getLabel(),c.getLabel().length());
        }

        for(Column c : dataFrame){
            for(Object value : c){
                int stringSize = value.toString().length();
                if (stringSize > colsMaxWidth.get(c.getLabel()) ) {
                    colsMaxWidth.put(c.getLabel(),stringSize);
                }
            }
        }
    }

    // -------------- Méthodes ------------------

    // -------------- Méthodes d'utilisation ------------------

    // Méthode qui affiche le dataFrame dans son intégralité.
    public String getAllDataString(){
        return getDataFrameString(0,dataFrame.getNbRow()-1,false,false);
    }

    // Méthode permettant d'afficher uniquement les premières lignes d'un dataFrame.
    // Le nombre de lignes à afficher est donné en paramètre.
    public String getFirstRowsString(int nbRowsToPrint){
        return getDataFrameString(0,nbRowsToPrint-1,false,true);
    }

    // Méthode permettant d'afficher uniquement les dernières lignes d'un dataFrame.
    // Le nombre de lignes à afficher est donné en paramètre.
    public String getLastRowsString(int nbRowsToPrint){
        return getDataFrameString(dataFrame.getNbRow()-nbRowsToPrint,dataFrame.getNbRow()-1,true,false);
    }


    // -------------- Méthodes utilitaire ------------------

    // Méthode permettant d'afficher le dataFrame entre les indices donnés en paramètre.
    // Les boléens hasBefore et hasAfter permettent d'afficher une ligne de "..." à la place des données
    // avant ou après les données selectionnées pour mettre en évidence que tout le dataFrame n'est pas affiché.
    // Les indices des lignes du dataFrame sont affichés avant chaque ligne.
    private String getDataFrameString(int indexToStart, Integer indexToFinish, Boolean hasBefore, Boolean hasAfter) throws IndexOutOfBoundsException{
        if( indexToStart < 0 || indexToStart > dataFrame.getNbRow() || indexToFinish+1 > dataFrame.getNbRow() || indexToFinish < 0){
            throw new IndexOutOfBoundsException("Vous essayez d'accéder à des indices hors de la taille du dataFrame");
        }
        StringBuilder dataFrameString = new StringBuilder();
        int maxIndexLength = indexToFinish.toString().length();
        if(hasBefore || hasAfter){
            if(maxIndexLength < "...".length() ){
                maxIndexLength = "...".length();
            }
        }

        String header = getLabelsString(maxIndexLength+3);
        dataFrameString.append(header);
        dataFrameString.append("\n");

        // Print les données
        StringBuilder rowBuilder = new StringBuilder();

        if(hasBefore){
            rowBuilder.append(colSeparator);
            rowBuilder.append(" ").append( getCenteredValue(maxIndexLength,"...") ).append(" ");
            rowBuilder.append(colSeparator);
            for(Column c : dataFrame){
                String centeredValue = getCenteredValue(colsMaxWidth.get(c.getLabel()),"...");
                rowBuilder.append(" ").append( centeredValue ).append(" ");
                rowBuilder.append(colSeparator);
            }
            rowBuilder.append("\n");
            dataFrameString.append(rowBuilder.toString());
        }

        for(int i=indexToStart; i<= indexToFinish; i++){
            rowBuilder = new StringBuilder().append(colSeparator);
            rowBuilder.append(" ").append(getCenteredValue(maxIndexLength,Integer.toString(i) )).append(" ");
            rowBuilder.append(colSeparator);
            for(Column c : dataFrame){
                Object value = c.getData().get(i);
                if( value == "null" ){
                    value = " ";
                }
//                Object value = c.getData().get(i);
                String centeredValue = getCenteredValue(colsMaxWidth.get(c.getLabel()),value.toString());
                rowBuilder.append(" ").append( centeredValue ).append(" ");
                rowBuilder.append(colSeparator);
            }
            rowBuilder.append("\n");
            dataFrameString.append(rowBuilder.toString());
        }

        if(hasAfter){
            rowBuilder = new StringBuilder().append(colSeparator);
            rowBuilder.append(" ").append( getCenteredValue(maxIndexLength,"...") ).append(" ");
            rowBuilder.append(colSeparator);
            for(Column c : dataFrame){
                String centeredValue = getCenteredValue(colsMaxWidth.get(c.getLabel()),"...");
                rowBuilder.append(" ").append( centeredValue ).append(" ");
                rowBuilder.append(colSeparator);
            }
            rowBuilder.append("\n");
            dataFrameString.append(rowBuilder.toString());
        }

        addCharToStr(dataFrameString,rowSeparator,rowBuilder.toString().length() - 1);
        return dataFrameString.toString();
    }

    // Méthode permettant de créer une chaine de caractères contenant l'affichage
    // de la première ligne de l'affichage final : les labels.
    private String getLabelsString(int offset){
        // Print la première ligne des labels
        StringBuilder labelsString = new StringBuilder();
        StringBuilder topSeparator = new StringBuilder();
        addCharToStr(topSeparator,rowSeparator,offset);
        StringBuilder strLabelsBuilder = new StringBuilder().append(colSeparator);
        StringBuilder strLineSeparatorBuilder = new StringBuilder();

        for( Column c : dataFrame){
            String centeredLabel = getCenteredValue(colsMaxWidth.get(c.getLabel()),c.getLabel());
            strLabelsBuilder.append(" ").append( centeredLabel ).append(" ");
            strLabelsBuilder.append(colSeparator);
        }
        addCharToStr(strLineSeparatorBuilder,rowSeparator,strLabelsBuilder.toString().length());
        addCharToStr(topSeparator,rowSeparator,strLabelsBuilder.toString().length());

        addCharToStr(labelsString,' ',offset);
        labelsString.append(strLineSeparatorBuilder.toString());
        labelsString.append("\n");
        addCharToStr(labelsString,' ',offset);
        labelsString.append(strLabelsBuilder.toString());
        labelsString.append("\n");
        labelsString.append(topSeparator.toString());

        return labelsString.toString();
    }

    // Méthode permettant d'ajouter un certain nombre de fois, un caractère à une chaine de caractères en construction.
    private void addCharToStr(StringBuilder strBuilder, char c, Integer nbCharToAdd){
        for(int i=0; i<nbCharToAdd; i++){
            strBuilder.append(c);
        }
    }

    // Méthode permettant d'obtenir un affichage centré d'une valeur d'une colonne pour l'affichage.
    private String getCenteredValue(Integer maxColWidth,String valueToCenter){
        StringBuilder centeredString = new StringBuilder();

        int preOffset = ( maxColWidth / 2 ) - ( valueToCenter.length() / 2 );
        int fill = preOffset;
        if( valueToCenter.length() % 2 == 0 && maxColWidth % 2 == 1){
            preOffset++;
        }else if( valueToCenter.length() % 2 == 1 && maxColWidth % 2 == 0){
            fill--;
        }
        addCharToStr(centeredString,' ',preOffset);
        centeredString.append(valueToCenter);
        addCharToStr(centeredString,' ',fill);
        return centeredString.toString();
    }

}
