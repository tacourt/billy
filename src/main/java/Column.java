import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Column implements Iterable<Object> {
    private String label;
    private List data;

    // Constructeur de colonne, il prend une structure de donnée simple, non typée, et se charge d'instancier le
    // bon type de conteneur en fonction des données qui lui sont fournies.
    public Column(List<Object> objectList) throws WrongTypeException, EmptyFileException {

        //Initialisation des variables
        List ret = null;
        int numNullBeforeValue=0;
        boolean continu = true;
        initLabel(objectList.get(0));
        objectList.remove(0);
        if(objectList.isEmpty()) throw new EmptyFileException("Erreur: Il semblerait que votre dataframe ne contienne que des labels");
        boolean isColEmpty = false;

        // Boucle utilisée dans le cas d'une colonne commençant par des valeurs absentes.
        while(continu) {
            continu = false;

            // Si la colonne commence par des valeurs absentes, on la parcourt jusqu'à trouver
            // la première valeur tout en comptant le nombre de valeurs absentes
            if (objectList.get(0) == "null"){
                numNullBeforeValue++;
                continu = true;
                objectList.remove(0);
                if(objectList.isEmpty()){
                    continu = false;
                    isColEmpty = true;
                }

            // Pour chaque type de donnée qui peut être rencontré dans la colonne,
            // on instancie le bon type de liste, pour y stocker les valeurs.
            } else if (objectList.get(0) instanceof Integer) {
                ret = new LinkedList<Integer>();
                while(numNullBeforeValue != 0) {
                    ret.add("null");
                    numNullBeforeValue--;
                }
                for (Object o : objectList) {
                    if (o instanceof Integer || o == "null") {
                        ret.add(o);
                    } else {
                        throw new WrongTypeException("Tous les elements de la colonne ne sont pas du même type.");
                    }
                }
            }else if (objectList.get(0) instanceof Float) {
                ret = new LinkedList<Float>();
                while(numNullBeforeValue != 0) {
                    ret.add("null");
                    numNullBeforeValue--;
                }
                for (Object o : objectList) {
                    if (o instanceof Float || o == "null") {
                        ret.add(o);
                    } else {
                        throw new WrongTypeException("Tous les elements de la colonne ne sont pas du même type.");
                    }
                }
            }else if (objectList.get(0) instanceof String) {
                ret = new LinkedList<String>();
                while(numNullBeforeValue != 0) {
                    ret.add("null");
                    numNullBeforeValue--;
                }
                ret.addAll(objectList);
            } else {
                throw new WrongTypeException("Type de la colonne non pris en charge");
            }
        }

        // Si la colonne est totalement vide, on instancie une liste de type String
        // et on la remplit de valeurs absentes.
        if(isColEmpty){
            ret = new LinkedList<String>();
            while(numNullBeforeValue != 0) {
                ret.add("null");
                numNullBeforeValue--;
            }
        }

        this.data = ret;
    }



    // Méthode d'initialisation des labels des colonnes contenus dans la liste non typée donnée.
    private void initLabel(Object firstObj) throws WrongTypeException {
        if(firstObj instanceof String) {
            this.label = (String) firstObj;
        }else {
            throw new WrongTypeException("La première case de la colonne n'est pas une chaîne de caractère");
        }
    }




    // Méthode permettant de concaténer deux colonnes, en plaçant celle donnée en
    // paramètre à la suite de la colonne sur laquelle est appelée cette méthode.
    public Column append(Column externalColumn, String label) throws WrongTypeException, EmptyFileException {
        List<Object> newColData = new LinkedList<>();
        newColData.add(label);
        newColData.addAll(data);
        for( Object value : externalColumn){
            newColData.add(value);
        }
        Column newCol = null;
        newCol = new Column(newColData);
        return newCol;
    }




    // Méthode permettant d'étendre la taille de la colonne pour atteindre la taille renseignée en paramètre.
    // Les nouvelles valeurs ajoutées sont vides.
    public void extendToFitSize(Integer sizeToFit) throws DimensionErrorException{
        if ( sizeToFit < data.size() ){
            throw new DimensionErrorException("La taille à atteindre est plus petite que la taille initiale de la colonne");
        }else{
            while (data.size() < sizeToFit){
                data.add("null");
            }
        }
    }



    // Méthode retournant la taille de la colonne / le nombre de valeurs contenues.
    public Integer size(){
        return data.size();
    }


    // Getter donnant le label attribué à la colonne
    public String getLabel(){
        return this.label;
    }


    // Getter retournant les données de la colonne
    public List getData(){
        return this.data;
    }


    // Méthode fournissant un itérateur pour parcourir la colonne
    @Override
    public Iterator<Object> iterator() {
        return data.iterator();
    }


    // Méthode permettant d'obtenir une liste d'objets correspondant à la colonne
    public List<Object> toObjectList(){
        List<Object> objectList = new ArrayList<>();
        objectList.add(this.getLabel());
        objectList.addAll(data);
        return objectList;
    }


    // Méthode d'affichage d'une colonne
    @Override
    public String toString() {
        StringBuilder res = new StringBuilder(label);
        for(Object o : data) {
            res.append(" | ").append(o.toString());
        }
        return res.toString();
    }


    // Méthode de comparaison de colonnes. Permet de renseigner si deux colonnes sont égales ou non.
    @Override
    public boolean equals(Object c) {
        if (!(c instanceof Column)) return false;
        Column cast = (Column)c;
        if(cast.data.size() != this.data.size()) return false;
        boolean res = this.label.equals(cast.label);
        for(int i=0; i<cast.data.size(); i++) {
            res = res && (cast.data.get(i).equals(this.data.get(i)));
        }
        return res;
    }
}
