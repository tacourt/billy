import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.*;

import static org.junit.Assert.*;

public class TestDataFrame {

    List<List<Object>> test;
    List<Object> column;
    @Before
    public void init() {
        test = new ArrayList<>();
        column = new ArrayList<>();
        column.add("test");
        column.add("test");
        test.add(column);
    }

    @Test
    public void initDataTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, IOException {
        DataFrame dataFrame = new DataFrame(test);
        List<List<Object>> listTest = dataFrame.readCSV("src/test/resources/Goodtest.csv");
        List<Column> expected = new ArrayList<>();
        List<Object> l1 = new ArrayList<>();
        List<Object> l2 = new ArrayList<>();
        List<Object> l3 = new ArrayList<>();
        l1.add("Prenom");
        l1.add("Tanguy");
        l1.add("Mathieu");
        l1.add("Jerome");
        l2.add("Age");
        l2.add(24);
        l2.add(22);
        l2.add("null");
        l3.add("TestDuNull");
        l3.add("null");
        l3.add("OUI");
        l3.add("NON");
        Column co1 = new Column(l1);
        Column co2 = new Column(l2);
        Column co3 = new Column(l3);
        expected.add(co1);
        expected.add(co2);
        expected.add(co3);
        dataFrame.initData(listTest);
        Iterator<Column> it = dataFrame.iterator();
        Iterator<Column> itExpected = expected.iterator();
        boolean res = true;
        while(it.hasNext()) {
            res = res && it.next().equals(itExpected.next());
        }
        assertTrue(res);
    }

    @Test
    public void readCSVTest() throws WrongTypeException, DimensionErrorException, EmptyFileException, IOException {
        List<List<Object>> expected = new ArrayList<>();
        List<Object> column1 = new ArrayList<>();
        List<Object> column2 = new ArrayList<>();
        List<Object> column3 = new ArrayList<>();
        column1.add("Prenom");
        column1.add("Tanguy");
        column1.add("Mathieu");
        column1.add("Jerome");
        column2.add("Age");
        column2.add(24);
        column2.add(22);
        column2.add("null");
        column3.add("TestDuNull");
        column3.add("null");
        column3.add("OUI");
        column3.add("NON");
        expected.add(column1);
        expected.add(column2);
        expected.add(column3);
        DataFrame dataFrame = new DataFrame(test);
        assertEquals(dataFrame.readCSV("src/test/resources/Goodtest.csv"), expected);
    }

    @Test (expected = EmptyFileException.class)
    public void readCSVTestEmptyFileExceptionTest() throws WrongTypeException, DimensionErrorException, EmptyFileException, IOException {
        DataFrame dataFrame = new DataFrame(test);
        dataFrame.readCSV("src/test/resources/EmptyTest.csv");
    }

    @Test (expected = IOException.class)
    public void readCSVTestIOExceptionTest() throws WrongTypeException, DimensionErrorException, EmptyFileException, IOException {
        DataFrame dataFrame = new DataFrame(test);
        dataFrame.readCSV("WrongPath");
    }

    @Test
    public void instanciateMatchingTypeStringTest() throws WrongTypeException, DimensionErrorException, EmptyFileException {
        DataFrame dataFrame = new DataFrame(test);
        Object o = dataFrame.instanciateMatchingType("Yo, ça va ?");
        assertTrue(o instanceof String);
    }

    @Test
    public void instanciateMatchingTypeIntTest() throws WrongTypeException, DimensionErrorException, EmptyFileException {
        DataFrame dataFrame = new DataFrame(test);
        Object o = dataFrame.instanciateMatchingType("156");
        assertTrue(o instanceof Integer);
    }

    @Test
    public void instanciateMatchingTypeFloatTest() throws WrongTypeException, DimensionErrorException, EmptyFileException {
        DataFrame dataFrame = new DataFrame(test);
        Object o = dataFrame.instanciateMatchingType("2.55");
        assertTrue(o instanceof Float);
    }

    @Test
    public void selectByRowTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, IOException {
        DataFrame dataFrame = new DataFrame("src/test/resources/Goodtest.csv");
        DataFrame dataFrameExpected = new DataFrame("src/test/resources/GoodtestTwoRows.csv");
        System.out.println(dataFrameExpected.toString());
        List<Integer> liste = new ArrayList<>();
        liste.add(0);
        liste.add(1);
        DataFrame actual = dataFrame.selectByRow(liste);

        //System.out.println(actual.toString());
        assertEquals(dataFrameExpected, actual);
    }

    @Test
    public void selectByColumnTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, IOException {
        DataFrame dataFrame = new DataFrame("src/test/resources/Goodtest.csv");
        DataFrame dataFrameExpected = new DataFrame("src/test/resources/GoodtestTwoColumns.csv");
        List<String> liste = new ArrayList<>();
        liste.add("Prenom");
        liste.add("Age");
        DataFrame actual = dataFrame.selectByColumn(liste);
        assertEquals(dataFrameExpected, actual);
    }

    @Test
    public void getNumColumnTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, IOException {
        DataFrame dataFrame = new DataFrame("src/test/resources/Goodtest.csv");
        assertEquals(3, dataFrame.getNbColumn());
    }

    @Test
    public void getNumRowTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, IOException {
        DataFrame dataFrame = new DataFrame("src/test/resources/Goodtest.csv");
        assertEquals(3, dataFrame.getNbRow());
    }

    @Test
    public void equalsTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, IOException {
        DataFrame expected = new DataFrame("src/test/resources/Goodtest.csv");
        DataFrame df = new DataFrame("src/test/resources/Goodtest.csv");
        assertEquals(df, expected);
    }

    @Test
    public void equalsAnotherTest() throws WrongTypeException, EmptyFileException, DimensionErrorException {
        List<List<Object>> df1 = new ArrayList<>();
        List<Object> column11 = new ArrayList<>();
        List<Object> column21 = new ArrayList<>();
        column11.add("Prenom");
        column11.add("Tanguy");
        column11.add("Mathieu");
        column11.add("Jerome");
        column21.add("Age");
        column21.add(24);
        column21.add(22);
        column21.add(21);
        df1.add(column11);
        df1.add(column21);
        DataFrame dataFrame1 = new DataFrame(df1);

        List<List<Object>> df2 = new ArrayList<>();
        List<Object> column2 = new ArrayList<>();
        List<Object> column3 = new ArrayList<>();
        column2.add("Prenom");
        column2.add("Tanguy");
        column2.add("Mathieu");
        column2.add("Jerome");
        column3.add("Age");
        column3.add(24);
        column3.add(22);
        column3.add(21);
        df2.add(column2);
        df2.add(column3);
        DataFrame dataFrame2 = new DataFrame(df2);
        assertEquals(dataFrame1, dataFrame2);
    }

    @Test
    public void mergeSameSizesTest() throws WrongTypeException, EmptyFileException, DimensionErrorException {
        List<List<Object>> expected = new ArrayList<>();
        List<Object> column1 = new ArrayList<>();
        List<Object> column2 = new ArrayList<>();
        List<Object> column3 = new ArrayList<>();
        List<Object> column4 = new ArrayList<>();
        column1.add("Prenom");
        column1.add("Tanguy");
        column1.add("Mathieu");
        column1.add("Jerome");
        column2.add("Age");
        column2.add(24);
        column2.add(22);
        column2.add(21);
        column3.add("Taille");
        column3.add(180);
        column3.add(186);
        column3.add(170);
        column4.add("Nom");
        column4.add("Court");
        column4.add("Gautron");
        column4.add("Pelissier");
        expected.add(column1);
        expected.add(column2);
        expected.add(column3);
        expected.add(column4);
        DataFrame expectedDataFrame = new DataFrame(expected);

        List<List<Object>> df1 = new ArrayList<>();
        List<Object> column11 = new ArrayList<>();
        List<Object> column21 = new ArrayList<>();
        column11.add("Prenom");
        column11.add("Tanguy");
        column11.add("Mathieu");
        column11.add("Jerome");
        column21.add("Age");
        column21.add(24);
        column21.add(22);
        column21.add(21);
        df1.add(column11);
        df1.add(column21);
        DataFrame dataFrame1 = new DataFrame(df1);

        List<List<Object>> df2 = new ArrayList<>();
        List<Object> column32 = new ArrayList<>();
        List<Object> column42 = new ArrayList<>();
        column32.add("Taille");
        column32.add(180);
        column32.add(186);
        column32.add(170);
        column42.add("Nom");
        column42.add("Court");
        column42.add("Gautron");
        column42.add("Pelissier");
        df2.add(column32);
        df2.add(column42);
        DataFrame dataFrame2 = new DataFrame(df2);

        DataFrame mergedDataFrame = dataFrame1.merge(dataFrame2);
        System.out.println(mergedDataFrame);
        System.out.println(expectedDataFrame);

        assertEquals(expectedDataFrame, mergedDataFrame);
    }

    @Test
    public void mergeDifferentSizesTest() throws WrongTypeException, EmptyFileException, DimensionErrorException {
        List<List<Object>> expected = new ArrayList<>();
        List<Object> column1 = new ArrayList<>();
        List<Object> column2 = new ArrayList<>();
        List<Object> column3 = new ArrayList<>();
        List<Object> column4 = new ArrayList<>();
        column1.add("Prenom");
        column1.add("Tanguy");
        column1.add("Mathieu");
        column1.add("Jerome");
        column1.add("Jean");
        column2.add("Age");
        column2.add(24);
        column2.add(22);
        column2.add(21);
        column2.add(25);
        column3.add("Taille");
        column3.add(180);
        column3.add(186);
        column3.add(170);
        column3.add("null");
        column4.add("Nom");
        column4.add("Court");
        column4.add("Gautron");
        column4.add("Pelissier");
        column4.add("null");
        expected.add(column1);
        expected.add(column2);
        expected.add(column3);
        expected.add(column4);
        DataFrame expectedDataFrame = new DataFrame(expected);

        List<List<Object>> df1 = new ArrayList<>();
        List<Object> column11 = new ArrayList<>();
        List<Object> column21 = new ArrayList<>();
        column11.add("Prenom");
        column11.add("Tanguy");
        column11.add("Mathieu");
        column11.add("Jerome");
        column11.add("Jean");
        column21.add("Age");
        column21.add(24);
        column21.add(22);
        column21.add(21);
        column21.add(25);
        df1.add(column11);
        df1.add(column21);
        DataFrame dataFrame1 = new DataFrame(df1);

        List<List<Object>> df2 = new ArrayList<>();
        List<Object> column32 = new ArrayList<>();
        List<Object> column42 = new ArrayList<>();
        column32.add("Taille");
        column32.add(180);
        column32.add(186);
        column32.add(170);
        column42.add("Nom");
        column42.add("Court");
        column42.add("Gautron");
        column42.add("Pelissier");
        df2.add(column32);
        df2.add(column42);
        DataFrame dataFrame2 = new DataFrame(df2);

        DataFrame mergedDataFrame = dataFrame1.merge(dataFrame2);
        System.out.println(mergedDataFrame);
        System.out.println(expectedDataFrame);

        assertEquals(expectedDataFrame, mergedDataFrame);
    }

    @Test
    public void mergeSameLabelsTest() throws WrongTypeException, EmptyFileException, DimensionErrorException {
        List<List<Object>> expected = new ArrayList<>();
        List<Object> column1 = new ArrayList<>();
        List<Object> column2 = new ArrayList<>();
        List<Object> column3 = new ArrayList<>();
        column1.add("Prenom");
        column1.add("Tanguy");
        column1.add("Mathieu");
        column1.add("Jerome");
        column1.add("Jean");
        column1.add("null");
        column1.add("null");
        column1.add("null");
        column2.add("Age");
        column2.add(24);
        column2.add(22);
        column2.add(21);
        column2.add(25);
        column2.add(30);
        column2.add(31);
        column2.add(32);
        column3.add("Taille");
        column3.add("null");
        column3.add("null");
        column3.add("null");
        column3.add("null");
        column3.add(180);
        column3.add(186);
        column3.add(170);
        expected.add(column1);
        expected.add(column2);
        expected.add(column3);
        DataFrame expectedDataFrame = new DataFrame(expected);

        List<List<Object>> df1 = new ArrayList<>();
        List<Object> column11 = new ArrayList<>();
        List<Object> column21 = new ArrayList<>();
        column11.add("Prenom");
        column11.add("Tanguy");
        column11.add("Mathieu");
        column11.add("Jerome");
        column11.add("Jean");
        column21.add("Age");
        column21.add(24);
        column21.add(22);
        column21.add(21);
        column21.add(25);
        df1.add(column11);
        df1.add(column21);
        DataFrame dataFrame1 = new DataFrame(df1);

        List<List<Object>> df2 = new ArrayList<>();
        List<Object> column32 = new ArrayList<>();
        List<Object> column42 = new ArrayList<>();
        column32.add("Taille");
        column32.add(180);
        column32.add(186);
        column32.add(170);
        column42.add("Age");
        column42.add(30);
        column42.add(31);
        column42.add(32);
        df2.add(column32);
        df2.add(column42);
        DataFrame dataFrame2 = new DataFrame(df2);

        DataFrame mergedDataFrame = dataFrame1.merge(dataFrame2);
        System.out.println(mergedDataFrame);
        System.out.println(expectedDataFrame);

        assertEquals(expectedDataFrame, mergedDataFrame);
    }

    @Test
    public void maxTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, IOException {
        DataFrame df = new DataFrame("src/test/resources/maxTest.csv");
        List<String> label = new ArrayList<>();
        label.add("Data1");
        label.add("Data2");
        label.add("Data3");
        List<Float> actual = df.maxByColumn(label);
        List<Float> expected = new ArrayList<>();
        expected.add(8f);
        expected.add(5.5f);
        expected.add(9f);
        assertEquals(expected, actual);
    }

    @Test (expected = WrongTypeException.class)
    public void maxWrongLabelTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, IOException {
        DataFrame df = new DataFrame("src/test/resources/maxTest.csv");
        List<String> label = new ArrayList<>();
        label.add("Data1");
        label.add("Data2");
        label.add("Blabla");
        df.maxByColumn(label);
    }

    @Test
    public void minTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, IOException {
        DataFrame df = new DataFrame("src/test/resources/maxTest.csv");
        List<String> label = new ArrayList<>();
        label.add("Data1");
        label.add("Data2");
        label.add("Data3");
        List<Float> actual = df.minByColumn(label);
        List<Float> expected = new ArrayList<>();
        expected.add(2f);
        expected.add(1.1f);
        expected.add(1f);
        assertEquals(expected, actual);
    }

    @Test (expected = WrongTypeException.class)
    public void minWrongLabelTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, IOException {
        DataFrame df = new DataFrame("src/test/resources/maxTest.csv");
        List<String> label = new ArrayList<>();
        label.add("Data1");
        label.add("Data2");
        label.add("Blabla");
        df.minByColumn(label);
    }

    @Test
    public void averageTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, IOException {
        DataFrame df = new DataFrame("src/test/resources/maxTest.csv");
        List<String> label = new ArrayList<>();
        label.add("Data1");
        label.add("Data2");
        label.add("Data3");
        List<Float> actual = df.averageByColumn(label);
        List<Float> expected = new ArrayList<>();
        expected.add(13.0f/3.0f);
        expected.add(9.8f/3.0f);
        expected.add(14.0f/3.0f);
        assertEquals(expected, actual);
    }

    @Test (expected = WrongTypeException.class)
    public void averageWrongLabelTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, IOException {
        DataFrame df = new DataFrame("src/test/resources/maxTest.csv");
        List<String> label = new ArrayList<>();
        label.add("Data1");
        label.add("Data2");
        label.add("Blabla");
        df.averageByColumn(label);
    }

    @Test
    public void ilocWithTwoListsTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, IOException {
        DataFrame expected = new DataFrame("src/test/resources/GoodtestTwoRowsTwoCols.csv");
        DataFrame df = new DataFrame("src/test/resources/Goodtest.csv");
        List<Integer> selectedRows = Arrays.asList(0,1);
        List<Integer> selectedCols = Arrays.asList(0,1);
        DataFrame actualDF = df.iloc(selectedRows,selectedCols);
        assertEquals(expected,actualDF);
    }

    @Test
    public void ilocWithListIntegerTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, IOException {
        DataFrame dfTest = new DataFrame("src/test/resources/Goodtest.csv");
        List<Integer> selectedRows = Arrays.asList(0);
        List<Integer> selectedCols = Arrays.asList(0,1);
        DataFrame expected = dfTest.iloc(selectedRows,selectedCols);
        DataFrame actualDF = dfTest.iloc(0,selectedCols);
        assertEquals(expected,actualDF);
    }

    @Test
    public void ilocWithIntegerListTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, IOException {
        DataFrame dfTest = new DataFrame("src/test/resources/Goodtest.csv");
        List<Integer> selectedRows = Arrays.asList(0,1);
        List<Integer> selectedCols = Arrays.asList(0);
        DataFrame expected = dfTest.iloc(selectedRows,selectedCols);
        DataFrame actualDF = dfTest.iloc(selectedRows,0);
        assertEquals(expected,actualDF);
    }

    @Test
    public void ilocWithIntegerIntegerTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, IOException {
        DataFrame dfTest = new DataFrame("src/test/resources/Goodtest.csv");
        List<Integer> selectedRows = Arrays.asList(0);
        List<Integer> selectedCols = Arrays.asList(0);
        DataFrame expected = dfTest.iloc(selectedRows,selectedCols);
        DataFrame actualDF = dfTest.iloc(0,0);
        assertEquals(expected,actualDF);
    }

    @Test
    public void getListOfNumberTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, IOException {
        DataFrame dfTest = new DataFrame("src/test/resources/Goodtest.csv");
        List<Integer> actual = dfTest.getListOfNumberFromTo(2,5);
        List<Integer> expected = Arrays.asList(2,3,4,5);
        assertEquals(expected,actual);
    }

    @Test
    public void getIndexsFromBeforeStringTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, WrongFormatException, IOException {
        DataFrame dfTest = new DataFrame("src/test/resources/Goodtest.csv");
        List<Integer> actual = dfTest.getIndexsFromSelectionString("1:",true);
        List<Integer> expected = Arrays.asList(1,2);
        assertEquals(expected,actual);
    }

    @Test
    public void getIndexsFromAfterStringTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, WrongFormatException, IOException {
        DataFrame dfTest = new DataFrame("src/test/resources/Goodtest.csv");
        List<Integer> actual = dfTest.getIndexsFromSelectionString(":2",true);
        List<Integer> expected = Arrays.asList(0,1,2);
        assertEquals(expected,actual);
    }

    @Test
    public void getIndexsFromBeforeAndAfterStringTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, WrongFormatException, IOException {
        DataFrame dfTest = new DataFrame("src/test/resources/Goodtest.csv");
        List<Integer> actual = dfTest.getIndexsFromSelectionString("1:3",true);
        List<Integer> expected = Arrays.asList(1,2,3);
        assertEquals(expected,actual);
    }

    @Test
    public void getIndexsFromBeforeNegativeStringTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, WrongFormatException, IOException {
        DataFrame dfTest = new DataFrame("src/test/resources/Goodtest.csv");
        List<Integer> actual = dfTest.getIndexsFromSelectionString("-2:",true);
        List<Integer> expected = Arrays.asList(1,2);
        assertEquals(expected,actual);
    }

    @Test
    public void getIndexsFromAfterNegativeStringTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, WrongFormatException, IOException {
        DataFrame dfTest = new DataFrame("src/test/resources/Goodtest.csv");
        List<Integer> actual = dfTest.getIndexsFromSelectionString(":-2",true);
        List<Integer> expected = Arrays.asList(0,1);
        assertEquals(expected,actual);
    }

    @Test
    public void getIndexsFromBeforeAndAfterNegativeStringTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, WrongFormatException, IOException {
        DataFrame dfTest = new DataFrame("src/test/resources/Goodtest.csv");
        List<Integer> actual = dfTest.getIndexsFromSelectionString("-1:-2",true);
        List<Integer> expected = Arrays.asList(1,2);
        assertEquals(expected,actual);
    }

    @Test
    public void getIndexsFromBeforeAndAfterNegativeStringOnColsTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, WrongFormatException, IOException {
        DataFrame dfTest = new DataFrame("src/test/resources/Goodtest.csv");
        List<Integer> actual = dfTest.getIndexsFromSelectionString("-1:-2",false);
        List<Integer> expected = Arrays.asList(1,2);
        assertEquals(expected,actual);
    }

    @Test
    public void getIndexsFromAllTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, WrongFormatException, IOException {
        DataFrame dfTest = new DataFrame("src/test/resources/Goodtest.csv");
        List<Integer> actual = dfTest.getIndexsFromSelectionString(":",true);
        List<Integer> expected = Arrays.asList(0,1,2);
        assertEquals(expected,actual);
    }

    @Test (expected = WrongFormatException.class)
    public void getIndexsWrongFormatExceptionTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, WrongFormatException, IOException {
        DataFrame dfTest = new DataFrame("src/test/resources/Goodtest.csv");
        dfTest.getIndexsFromSelectionString("MauvaisFormat",true);
    }


    @Test
    public void ilocWithSelectionStringListTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, WrongFormatException, IOException {
        DataFrame dfTest = new DataFrame("src/test/resources/Goodtest.csv");
        List<Integer> selectedRows = Arrays.asList(1,2);
        List<Integer> selectedCols = Arrays.asList(0,1);
        DataFrame expected = dfTest.iloc(selectedRows,selectedCols);
        DataFrame actualDF = dfTest.iloc("1:2",selectedCols);
        assertEquals(expected,actualDF);
    }

    @Test
    public void ilocWithSelectionStringIntegerTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, WrongFormatException, IOException {
        DataFrame dfTest = new DataFrame("src/test/resources/Goodtest.csv");
        List<Integer> selectedRows = Arrays.asList(1,2);
        List<Integer> selectedCols = Arrays.asList(0);
        DataFrame expected = dfTest.iloc(selectedRows,selectedCols);
        DataFrame actualDF = dfTest.iloc("1:2",0);
        assertEquals(expected,actualDF);
    }

    @Test
    public void ilocWithListSelectionStringTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, WrongFormatException, IOException {
        DataFrame dfTest = new DataFrame("src/test/resources/Goodtest.csv");
        List<Integer> selectedRows = Arrays.asList(1,2);
        List<Integer> selectedCols = Arrays.asList(1,2);
        DataFrame expected = dfTest.iloc(selectedRows,selectedCols);
        DataFrame actualDF = dfTest.iloc(selectedRows,"1:2");
        assertEquals(expected,actualDF);
    }

    @Test
    public void ilocWithIntegerSelectionStringTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, WrongFormatException, IOException {
        DataFrame dfTest = new DataFrame("src/test/resources/Goodtest.csv");
        List<Integer> selectedRows = Arrays.asList(1);
        List<Integer> selectedCols = Arrays.asList(1,2);
        DataFrame expected = dfTest.iloc(selectedRows,selectedCols);
        DataFrame actualDF = dfTest.iloc(1,"1:2");
        assertEquals(expected,actualDF);
    }

    @Test
    public void ilocWithTwoSelectionStringTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, WrongFormatException, IOException {
        DataFrame dfTest = new DataFrame("src/test/resources/Goodtest.csv");
        List<Integer> selectedRows = Arrays.asList(1,2);
        List<Integer> selectedCols = Arrays.asList(1,2);
        DataFrame expected = dfTest.iloc(selectedRows,selectedCols);
        DataFrame actualDF = dfTest.iloc("1:2","1:2");
        assertEquals(expected,actualDF);
    }

    @Test
    public void compareIntEqualsTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, WrongFormatException, IOException {
        DataFrame dfTest = new DataFrame("src/test/resources/Goodtest.csv");
        Boolean value = dfTest.compare(2,"==",2);
        assertTrue(value);
    }

    @Test
    public void compareIntSuperiorTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, WrongFormatException, IOException {
        DataFrame dfTest = new DataFrame("src/test/resources/Goodtest.csv");
        Boolean value = dfTest.compare(5,">",2);
        assertTrue(value);
    }

    @Test
    public void compareIntInferiorTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, WrongFormatException, IOException {
        DataFrame dfTest = new DataFrame("src/test/resources/Goodtest.csv");
        Boolean value = dfTest.compare(2,"<",5);
        assertTrue(value);
    }

    @Test
    public void compareIntFalseTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, WrongFormatException, IOException {
        DataFrame dfTest = new DataFrame("src/test/resources/Goodtest.csv");
        Boolean value = dfTest.compare(3,"==",2);
        assertFalse(value);
    }

    @Test
    public void compareSuperiorOrEqualTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, WrongFormatException, IOException {
        DataFrame dfTest = new DataFrame("src/test/resources/Goodtest.csv");
        Boolean value = dfTest.compare(2,"<=",2);
        assertTrue(value);
    }

    @Test
    public void compareInferiorOrEqualTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, WrongFormatException, IOException {
        DataFrame dfTest = new DataFrame("src/test/resources/Goodtest.csv");
        Boolean value = dfTest.compare(2,">=",2);
        assertTrue(value);
    }

    @Test (expected = WrongFormatException.class)
    public void compareWrongOperatorTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, WrongFormatException, IOException {
        DataFrame dfTest = new DataFrame("src/test/resources/Goodtest.csv");
        dfTest.compare(2,"FalseOp",2);
    }

    @Test (expected = WrongFormatException.class)
    public void compareWrongOperatorForStringTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, WrongFormatException, IOException {
        DataFrame dfTest = new DataFrame("src/test/resources/Goodtest.csv");
        dfTest.compare("value",">","value");
    }

    @Test
    public void compareDifferentTypeTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, WrongFormatException, IOException {
        DataFrame dfTest = new DataFrame("src/test/resources/Goodtest.csv");
        Boolean value = dfTest.compare(3,"==","value");
        assertFalse(value);
    }

    @Test
    public void compareDifferentTypeStringTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, WrongFormatException, IOException {
        DataFrame dfTest = new DataFrame("src/test/resources/Goodtest.csv");
        Boolean value = dfTest.compare("value","==",3);
        assertFalse(value);
    }

    @Test
    public void compareStringTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, WrongFormatException, IOException {
        DataFrame dfTest = new DataFrame("src/test/resources/Goodtest.csv");
        Boolean value = dfTest.compare("value","==","value");
        assertTrue(value);
    }

    @Test (expected = WrongFormatException.class)
    public void whereWrongLabelTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, WrongFormatException, IOException {
        DataFrame dfTest = new DataFrame("src/test/resources/Goodtest.csv");
        dfTest.where("colonne","==","value");
    }

    @Test
    public void whereUniqueTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, WrongFormatException, IOException {
        DataFrame dfTest = new DataFrame("src/test/resources/Goodtest.csv");
        DataFrame expected = dfTest.iloc(0,":");
        DataFrame actual = dfTest.where("Age",">",22);
        assertEquals(expected,actual);
    }

    @Test
    public void whereMultipleTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, WrongFormatException, IOException {
        DataFrame dfTest = new DataFrame("src/test/resources/Goodtest.csv");
        List<Integer> selectedRows = Arrays.asList(0,1);
        DataFrame expected = dfTest.iloc(selectedRows,":");
        DataFrame actual = dfTest.where("Age",">",20);
        assertEquals(expected,actual);
    }

    @Test
    public void printAllTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, WrongFormatException, IOException {
        DataFrame dfTest = new DataFrame("src/test/resources/Goodtest.csv");
        dfTest.printAll();
    }

    @Test
    public void printFirstTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, WrongFormatException, IOException {
        DataFrame dfTest = new DataFrame("src/test/resources/Goodtest.csv");
        dfTest.printFirst(2);
    }

    @Test
    public void printLastTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, WrongFormatException, IOException {
        DataFrame dfTest = new DataFrame("src/test/resources/Goodtest.csv");
        dfTest.printLast(2);
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void printFirstExceptionTest() throws WrongTypeException, EmptyFileException, DimensionErrorException, WrongFormatException, IOException {
        DataFrame dfTest = new DataFrame("src/test/resources/Goodtest.csv");
        dfTest.printLast(50);
    }



}
