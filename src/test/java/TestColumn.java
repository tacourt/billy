import org.junit.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.*;

public class TestColumn {

    @Test(expected = EmptyFileException.class)
    public void constructorEmptyFileExceptionTest() throws EmptyFileException, WrongTypeException {
        List<Object> label = new ArrayList<>();
        label.add("Label");
        new Column(label);
    }

    @Test(expected = WrongTypeException.class)
    public void constructorWrongTypeExceptionIntStartTest() throws EmptyFileException, WrongTypeException {
        List<Object> label = new ArrayList<>();
        label.add("Label");
        label.add(3);
        label.add("YO");
        new Column(label);
    }

    @Test(expected = WrongTypeException.class)
    public void constructorWrongTypeExceptionFloatStartTest() throws EmptyFileException, WrongTypeException {
        List<Object> label = new ArrayList<>();
        label.add("Label");
        label.add(5.7);
        label.add(3);
        new Column(label);
    }

    @Test(expected = WrongTypeException.class)
    public void constructorWrongTypeExceptionNullBetweenTest() throws EmptyFileException, WrongTypeException {
        List<Object> label = new ArrayList<>();
        label.add("Label");
        label.add(5.7);
        label.add("null");
        label.add(3);
        new Column(label);
    }

    @Test
    public void constructorNullBeforeTest() throws EmptyFileException, WrongTypeException {
        List<Object> label = new ArrayList<>();
        label.add("Label");
        label.add("null");
        label.add("null");
        label.add(3);
        List<Object> labelClone = new ArrayList<>();
        labelClone.add("Label");
        labelClone.add("null");
        labelClone.add("null");
        labelClone.add(3);
        Column test = new Column(labelClone);
        boolean res = true;
        Iterator<Object> it = test.iterator();
        int i=1;
        while(it.hasNext()) {
            Object o = it.next();
            res = res && label.get(i).equals(o);
            i++;
        }
        assertTrue(res);
    }

    @Test
    public void constructorNullAfterTest() throws EmptyFileException, WrongTypeException {
        List<Object> label = new ArrayList<>();
        label.add("Label");
        label.add(3);
        label.add("null");
        label.add("null");
        List<Object> labelClone = new ArrayList<>();
        labelClone.add("Label");
        labelClone.add(3);
        labelClone.add("null");
        labelClone.add("null");
        Column test = new Column(labelClone);
        boolean res = true;
        Iterator<Object> it = test.iterator();
        int i=1;
        while(it.hasNext()) {
            Object o = it.next();
            res = res && label.get(i).equals(o);
            i++;
        }
        assertTrue(res);
    }

    @Test
    public void constructorNullBetweenTest() throws EmptyFileException, WrongTypeException {
        List<Object> label = new ArrayList<>();
        label.add("Label");
        label.add(3);
        label.add("null");
        label.add(8);
        List<Object> labelClone = new ArrayList<>();
        labelClone.add("Label");
        labelClone.add(3);
        labelClone.add("null");
        labelClone.add(8);
        Column test = new Column(labelClone);
        boolean res = true;
        Iterator<Object> it = test.iterator();
        int i=1;
        while(it.hasNext()) {
            Object o = it.next();
            res = res && label.get(i).equals(o);
            i++;
        }
        assertTrue(res);
    }

    @Test (expected = WrongTypeException.class)
    public void constructorFloatWrongTypeExceptionTest() throws EmptyFileException, WrongTypeException {
        List<Object> col = new ArrayList<>();
        col.add("taille");
        col.add(3.2f);
        col.add(3.2f);
        col.add(3);
        new Column(col);
    }

    @Test (expected = WrongTypeException.class)
    public void initLabelExceptionTest() throws EmptyFileException, WrongTypeException {
        List<Object> col = new ArrayList<>();
        col.add(3.2f);
        new Column(col);
    }

    @Test
    public void getLabelTest() throws EmptyFileException, WrongTypeException {
        List<Object> label = new ArrayList<>();
        label.add("Label");
        label.add(3);
        Column test = new Column(label);
        assertEquals("Label", test.getLabel());
    }

    @Test
    public void equalsTest() throws EmptyFileException, WrongTypeException {
        List<Object> label = new ArrayList<>();
        label.add("Label");
        label.add(3);
        label.add("null");
        label.add(8);
        List<Object> labelClone = new ArrayList<>();
        labelClone.add("Label");
        labelClone.add(3);
        labelClone.add("null");
        labelClone.add(8);
        Column c = new Column(label);
        Column c2 = new Column(labelClone);
        assertEquals(c, c2);
    }

    @Test
    public void appendColumnsSameTypeTest() throws EmptyFileException, WrongTypeException {
        List<Object> taille = new ArrayList<>();
        taille.add("Taille");
        taille.add(12);
        taille.add(45);
        taille.add(32);
        Column columnTaille = new Column(taille);

        List<Object> prix = new ArrayList<>();
        prix.add("Prix");
        prix.add(30);
        prix.add(48);
        prix.add(35);
        Column columnPrix = new Column(prix);

        List<Object> expected = new ArrayList<>();
        expected.add("Prix");
        expected.add(30);
        expected.add(48);
        expected.add(35);
        expected.add(12);
        expected.add(45);
        expected.add(32);
        Column expectedColumn = new Column(expected);

        Column mergedColumn = columnPrix.append(columnTaille,columnPrix.getLabel());

        assertEquals(expectedColumn, mergedColumn);
    }

    @Test(expected = WrongTypeException.class)
    public void appendColumnsDifferentTypeTest() throws EmptyFileException, WrongTypeException {
        List<Object> taille = new ArrayList<>();
        taille.add("Taille");
        taille.add(12);
        taille.add(45);
        taille.add(32);
        Column columnTaille = new Column(taille);

        List<Object> prix = new ArrayList<>();
        prix.add("Prix");
        prix.add(30.2);
        prix.add(48.6);
        prix.add(35.5);
        Column columnPrix = new Column(prix);

        columnPrix.append(columnTaille,columnPrix.getLabel());
    }

    @Test
    public void fitToSizeTest() throws DimensionErrorException, EmptyFileException, WrongTypeException {
        List<Object> label = new ArrayList<>();
        label.add("Label");
        label.add(5);
        label.add(3);
        Column test = new Column(label);

        List<Object> expected = new ArrayList<>();
        expected.add("Label");
        expected.add(5);
        expected.add(3);
        expected.add("null");
        expected.add("null");
        Column expectedColumn = new Column(expected);

        test.extendToFitSize(4);
        System.out.println(test);
        System.out.println(expectedColumn);
        assertEquals(test, expectedColumn);
    }

    @Test
    public void fitToSizeSameTest() throws DimensionErrorException, EmptyFileException, WrongTypeException {
        List<Object> label = new ArrayList<>();
        label.add("Label");
        label.add(5);
        label.add(3);
        Column test = new Column(label);

        List<Object> expected = new ArrayList<>();
        expected.add("Label");
        expected.add(5);
        expected.add(3);
        Column expectedColumn = new Column(expected);

        test.extendToFitSize(2);
        System.out.println(test);
        System.out.println(expectedColumn);
        assertEquals(test, expectedColumn);
    }

    @Test (expected = DimensionErrorException.class)
    public void fitToSizeExceptionTest() throws DimensionErrorException, EmptyFileException, WrongTypeException {
        List<Object> label = new ArrayList<>();
        label.add("Label");
        label.add(5);
        label.add(3);
        Column test = new Column(label);
        test.extendToFitSize(1);
    }
}
